package ro.alexi.awbd.service;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.AwbdApplicationTests;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.dtos.paginationDTO.WarehousePaginationResponse;
import ro.alexi.awbd.mapper.WarehouseMapperImpl;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.SupplierRepository;
import ro.alexi.awbd.repository.WarehouseRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = AwbdApplicationTests.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class Warehouse_IntegrationTest {

    //@Mock - inlocuieste un fel de proxy over
    //@InjectMock - aduce o instanta reala a obiectului si ii si injecteaza alte mocuri dependente

    @InjectMocks
    WarehouseService warehouseService;

    @Autowired
    WarehouseRepository warehouseRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Mock
    WarehouseMapperImpl warehouseMapper = new WarehouseMapperImpl();


    private  Supplier supplier_to_insert;
    private  Warehouse warehouse_to_insert;
    @BeforeEach
    void setUp() {
        warehouseService = new WarehouseService(warehouseRepository, warehouseMapper);
        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));
        warehouse_to_insert.setSupplier(supplier_to_insert);

        supplierRepository.save(supplier_to_insert);
        warehouseRepository.save(warehouse_to_insert);
        List<Warehouse> warehouseDatabaseItems = warehouseRepository.findAll();
        boolean test_if_are_items_in_db_debug  = true;
    }

    @AfterEach
    void unset_TearDown() {
        supplierRepository.deleteAll();
        warehouseRepository.deleteAll();
    }
    // get Warehouse

    @Test
    @DisplayName("Check if Warehouse data is received")
    void getWarehouseInfo() throws Exception {
        //set - arrange ->date care reprezinta scenariul ce se va testa
        //am doar 1 warehouse in baza de date inserat
        Pageable pageable = PageRequest.of(1, 10);
        WarehousePaginationResponse initial, after;

        Page<Warehouse> warehousePage = warehouseRepository.findAll(pageable);
        WarehousePaginationResponse warehousePaginationResponse = new WarehousePaginationResponse();
        warehousePaginationResponse.setTotalItems(warehousePage.getTotalElements());
        warehousePaginationResponse.setWarehouses(warehouseMapper.toDto(warehousePage.getContent()));

        initial = warehousePaginationResponse;

        when(warehouseRepository.findById(any())).thenReturn(Optional.of(warehouse_to_insert));


        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehousePaginationResponse warehouseResponse = warehouseService.getWarehouses(pageable);
        after = warehouseResponse;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO

        assertEquals(
                initial.getTotalItems(),
                after.getTotalItems());
        assertEquals(
                initial.getWarehouses().stream().count(),
                after.getWarehouses().stream().count());
        assertEquals(
                initial.getWarehouses().get(0).getAddress(),
                after.getWarehouses().get(0).getAddress());
        assertEquals(
                initial.getWarehouses().get(0).getCity(),
                after.getWarehouses().get(0).getCity());
        assertEquals(
                initial.getWarehouses().get(0).getSupplier().getName(),
                after.getWarehouses().get(0).getSupplier().getName());
        assertEquals(
                initial.getWarehouses().get(0).getSupplier().getAddress(),
                after.getWarehouses().get(0).getSupplier().getAddress());
        assertEquals(
                initial.getWarehouses().get(0).getSupplier().getPostalCode(),
                after.getWarehouses().get(0).getSupplier().getPostalCode());

    }


    // save Warehouse


    // delete Warehouse


    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}