package ro.alexi.awbd.service;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.*;
import ro.alexi.awbd.dtos.paginationDTO.SupplierPaginationResponse;
import ro.alexi.awbd.dtos.paginationDTO.WarehousePaginationResponse;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.jwt.JwtTokenUtil;
import ro.alexi.awbd.mapper.*;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.model.Product;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.repository.SupplierRepository;
import ro.alexi.awbd.repository.WarehouseRepository;
import ro.alexi.awbd.security.SecurityUserService;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WarehouseMapperImpl.class})
class SupplierService_Test {

    //@Mock - inlocuieste un fel de proxy over
    //@InjectMock - aduce o instanta reala a obiectului si ii si injecteaza alte mocuri dependente

    @InjectMocks
    WarehouseService warehouseService;
    @InjectMocks
    SupplierService supplierService;

    @Mock
    WarehouseRepository warehouseRepository;

    @Mock
    SupplierRepository supplierRepository;

    @Mock
    WarehouseMapper warehouseMockMapper;
    @Mock
    SupplierMapper supplierMockMapper ;
    @Mock
    ProductMapper productMapper ;

    private WarehouseMapperImpl warehouseMapper = new WarehouseMapperImpl();

    private Supplier supplier_to_insert;
    private Warehouse warehouse_to_insert;
    private WarehouseDTO warehouse_DTO_to_insert;
    private SupplierDTO supplierDTO_to_insert;

    @BeforeEach
    void setUp() {
        warehouseService = new WarehouseService(warehouseRepository, warehouseMockMapper);
        supplierService = new SupplierService(supplierRepository, supplierMockMapper,warehouseMapper,productMapper);

    }

    @AfterEach
    void unset_TearDown() {
        supplier_to_insert = null;
        warehouse_to_insert = null;
        warehouse_DTO_to_insert = null;
        supplierDTO_to_insert = null;
    }


    // save Warehouse
    @Test
    @DisplayName("Check if Supplier data is saved - SUCCESS ")
    void saveWarehouseInfo() throws Exception {
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();

        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        supplierDTO_to_insert = new SupplierMapperImpl().toDto(supplier_to_insert);

        when(supplierRepository.findByNameAndAddress(any(), any())).thenReturn(Optional.of(supplier_to_insert));
        when(supplierRepository.save(any())).thenReturn(supplier_to_insert);
        when(supplierMockMapper.toDto(supplier_to_insert)).thenReturn(supplierDTO_to_insert);

          /*
         public SupplierDTO saveSupplier(SupplierDTO supplierDTO) {
        Optional<Supplier> optionalSupplier = supplierRepository.findByNameAndAddress(supplierDTO.getName(), supplierDTO.getAddress());

        if (optionalSupplier.isEmpty() || supplierDTO.getId() != null) {
            Supplier savedSupplier = supplierRepository.save(supplierMapper.toEntity(supplierDTO));
            return supplierMapper.toDto(savedSupplier);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Furnizorul exista deja");
        }
    }
         */
        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        SupplierDTO response = supplierService.saveSupplier(supplierDTO_to_insert);
        SupplierDTO initial = supplierDTO_to_insert;
        SupplierDTO after = response;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO
        verify(supplierRepository, times(1)).findByNameAndAddress(any(), any());
        verify(supplierRepository, times(1)).save(any());
        verify(supplierMockMapper, times(1)).toDto(supplier_to_insert);

        assertEquals(
                initial.getId(),
                after.getId());
        assertEquals(
                initial.getAddress(),
                after.getAddress());
        assertEquals(
                initial.getName(),
                after.getName());
        assertEquals(
                initial.getPostalCode(),
                after.getPostalCode());

    }

    @Test
    @DisplayName(value = "SAVE Supplier- ERROR TEST")
    public void SAVE_test_error(){
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        supplier_to_insert.setId(null);

        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        supplierDTO_to_insert = new SupplierMapperImpl().toDto(supplier_to_insert);

        when(supplierRepository.findByNameAndAddress(any(), any())).thenReturn(Optional.of(supplier_to_insert));

        /*
         public SupplierDTO saveSupplier(SupplierDTO supplierDTO) {
        Optional<Supplier> optionalSupplier = supplierRepository.findByNameAndAddress(supplierDTO.getName(), supplierDTO.getAddress());

        if (optionalSupplier.isEmpty() || supplierDTO.getId() != null) {
            Supplier savedSupplier = supplierRepository.save(supplierMapper.toEntity(supplierDTO));
            return supplierMapper.toDto(savedSupplier);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Furnizorul exista deja");
        }
    }
         */

        CustomException expectedException =  new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Furnizorul exista deja");

        //act + assert
        CustomException exceptionResult =
                assertThrows(CustomException.class, ()->{supplierService.saveSupplier(supplierDTO_to_insert);});
        assertEquals(expectedException.getMessage(), exceptionResult.getMessage());
        verify(supplierRepository, times(1)).findByNameAndAddress(any(), any());
    }
    // get Warehouse

    @Test
    @DisplayName("Get List Of Suppliers - data is received")
    void getSupplierInfo()  {
        //set - arrange ->date care reprezinta scenariul ce se va testa
        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));
        warehouse_to_insert.setSupplier(supplier_to_insert);

        supplierDTO_to_insert = new SupplierMapperImpl().toDto(supplier_to_insert);

        Pageable pageable = PageRequest.of(1, 1);
        SupplierPaginationResponse initial, after;

        Page<Supplier> supplierPage = new PageImpl<Supplier>(List.of(supplier_to_insert), pageable, pageable.getPageSize());
        SupplierPaginationResponse supplierPaginationResponse = new SupplierPaginationResponse();
        supplierPaginationResponse.setTotalItems(supplierPage.getTotalElements());
        supplierPaginationResponse.setSuppliers(supplierMockMapper.toDto(supplierPage.getContent()));
        supplierPaginationResponse.setTotalItems(1);

        initial = supplierPaginationResponse;

        when(supplierRepository.findAll()).thenReturn(List.of(supplier_to_insert));
        when(supplierMockMapper.toDto(supplier_to_insert)).thenReturn(supplierDTO_to_insert);//supplierMapper.toDto
        /*
         public SupplierPaginationResponse getSuppliers(Pageable pageable, boolean completeList) {
             SupplierPaginationResponse supplierPaginationResponse = new SupplierPaginationResponse();
             if (completeList) {
                 List<Supplier> all = supplierRepository.findAll();

                 supplierPaginationResponse.setTotalItems(all.size());
                 supplierPaginationResponse.setSuppliers(supplierMapper.toDto(all));
             } else {
                 Page<Supplier> suppliers = supplierRepository.findAll(pageable);

                 supplierPaginationResponse.setTotalItems(suppliers.getTotalElements());
                 supplierPaginationResponse.setSuppliers(supplierMapper.toDto(suppliers.getContent()));
             }
             return supplierPaginationResponse;
         }
         * */

        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        SupplierPaginationResponse supplierResponse = supplierService.getSuppliers(pageable,true);
        after = supplierResponse;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO

        verify(supplierRepository, times(1)).findAll();

        assertEquals(
                initial.getTotalItems(),
                after.getTotalItems());
        assertEquals(
                initial.getSuppliers().stream().count(),
                after.getSuppliers().stream().count());
    }


    // delete Warehouse

    @Test
    @DisplayName(value = "DELETE Supplier- ERROR TEST")
    public void DELETE_test_error(){
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        supplierDTO_to_insert = new SupplierMapperImpl().toDto(supplier_to_insert);

        when(supplierRepository.findById(any())).thenReturn(Optional.empty());
        /*
         public void deleteSupplier(UUID supplierId) {
            Optional<Supplier> byId = supplierRepository.findById(supplierId);

            if (byId.isPresent()) {
                supplierRepository.delete(byId.get());
            } else {
                throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Furnziorul nu a fost gasit");
            }
         }
         * */

        CustomException expectedException =  new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Furnziorul nu a fost gasit");
        //act + assert
        CustomException exceptionResult =
                assertThrows(CustomException.class, ()->{supplierService.deleteSupplier(supplierDTO_to_insert.getId());});
        assertEquals(expectedException.getMessage(), exceptionResult.getMessage());
        verify(supplierRepository, times(1)).findById(any());
    }

    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}