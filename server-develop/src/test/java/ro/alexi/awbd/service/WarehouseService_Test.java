package ro.alexi.awbd.service;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.*;
import ro.alexi.awbd.dtos.paginationDTO.WarehousePaginationResponse;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.jwt.JwtTokenUtil;
import ro.alexi.awbd.mapper.*;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.model.Product;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.repository.SupplierRepository;
import ro.alexi.awbd.repository.WarehouseRepository;
import ro.alexi.awbd.security.SecurityUserService;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WarehouseMapperImpl.class})
class WarehouseService_Test {

    //@Mock - inlocuieste un fel de proxy over
    //@InjectMock - aduce o instanta reala a obiectului si ii si injecteaza alte mocuri dependente

    @InjectMocks
    WarehouseService warehouseService;


    @Mock
    WarehouseRepository warehouseRepository;

    @Mock
    SupplierRepository supplierRepository;

    @Autowired
    WarehouseMapper warehouseMockMapper;

    private WarehouseMapperImpl warehouseMapper = new WarehouseMapperImpl();

    private Supplier supplier_to_insert;
    private Warehouse warehouse_to_insert;
    private WarehouseDTO warehouse_DTO_to_insert;
    private SupplierDTO supplierDTO_to_insert = new SupplierDTO();
    @BeforeEach
    void setUp() {
        warehouseService = new WarehouseService(warehouseRepository, warehouseMockMapper);
    }

    @AfterEach
    void unset_TearDown() {
        supplier_to_insert = null;
        warehouse_to_insert = null;
        warehouse_DTO_to_insert = null;
        supplierDTO_to_insert = null;
    }

    //convert Warehouse to DTO
    @Test
    @DisplayName("Check convert Warehouse to DTO - SUCCESS")
    void WarehouseToDTO() throws Exception {
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));
        warehouse_to_insert.setSupplier(supplier_to_insert);

        supplierDTO_to_insert.setId(supplier_to_insert.getId());
        supplierDTO_to_insert.setName(supplier_to_insert.getName());
        supplierDTO_to_insert.setAddress(supplier_to_insert.getAddress());
        supplierDTO_to_insert.setPostalCode(supplier_to_insert.getPostalCode());

        warehouse_DTO_to_insert = new WarehouseDTO();
        warehouse_DTO_to_insert.setId(warehouse_to_insert.getId());
        warehouse_DTO_to_insert.setCity(warehouse_to_insert.getCity());
        warehouse_DTO_to_insert.setAddress(warehouse_to_insert.getAddress());
        warehouse_DTO_to_insert.setSupplier(supplierDTO_to_insert);

        Warehouse initial;
        WarehouseDTO after;

        initial = warehouse_to_insert;

        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehouseDTO result = warehouseMockMapper.toDto(warehouse_to_insert);
        after = result;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO

        assertEquals(
                initial.getSupplier().getId(),
                after.getSupplier().getId());
        assertEquals(
                initial.getSupplier().getName(),
                after.getSupplier().getName());
        assertEquals(
                initial.getSupplier().getPostalCode(),
                after.getSupplier().getPostalCode());
        assertEquals(
                initial.getSupplier().getAddress(),
                after.getSupplier().getAddress());
    }

    //convert DTO TO Warehouse
    @Test
    @DisplayName("Check convert Warehouse to DTO - SUCCESS")
    void DTOToWarehouse(){
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));
        warehouse_to_insert.setSupplier(supplier_to_insert);

        supplierDTO_to_insert.setId(supplier_to_insert.getId());
        supplierDTO_to_insert.setName(supplier_to_insert.getName());
        supplierDTO_to_insert.setAddress(supplier_to_insert.getAddress());
        supplierDTO_to_insert.setPostalCode(supplier_to_insert.getPostalCode());

        warehouse_DTO_to_insert = new WarehouseDTO();
        warehouse_DTO_to_insert.setId(warehouse_to_insert.getId());
        warehouse_DTO_to_insert.setCity(warehouse_to_insert.getCity());
        warehouse_DTO_to_insert.setAddress(warehouse_to_insert.getAddress());
        warehouse_DTO_to_insert.setSupplier(supplierDTO_to_insert);

        WarehouseDTO initial;
        Warehouse after;
        initial = warehouse_DTO_to_insert;

        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        Warehouse result = warehouseMockMapper.toEntity(warehouse_DTO_to_insert);
        after = result;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO

        assertEquals(
                initial.getSupplier().getId(),
                after.getSupplier().getId());
        assertEquals(
                initial.getSupplier().getName(),
                after.getSupplier().getName());
        assertEquals(
                initial.getSupplier().getPostalCode(),
                after.getSupplier().getPostalCode());
        assertEquals(
                initial.getSupplier().getAddress(),
                after.getSupplier().getAddress());
    }


    // save Warehouse
    @Test
    @DisplayName("Check if Warehouse data is saved - SUCCESS -> need pass NO DUPLICATE ADDRESS & ")
    void saveWarehouseInfo() throws Exception {
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        warehouse_DTO_to_insert = warehouseMapper.toDto(warehouse_to_insert);
        WarehouseDTO initial, after;

        //warehouseRepository.findByAddress(warehouseDTO.getAddress());
        // warehouseByAddress.isEmpty() || warehouseDTO.getId() != null
        // warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));

        /**
         public WarehouseDTO saveWarehouse(WarehouseDTO warehouseDTO) {
         Optional<Warehouse> warehouseByAddress = warehouseRepository.findByAddress(warehouseDTO.getAddress());
         if (warehouseByAddress.isEmpty() || warehouseDTO.getId() != null) {
         Warehouse savedWarehouse = warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));
         return warehouseMapper.toDto(savedWarehouse);
         }
         throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
         }
         * */
        when(warehouseRepository.findByAddress(any())).thenReturn(Optional.empty());
        when(warehouseRepository.save(any())).thenReturn(warehouse_to_insert);

        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehouseDTO response = warehouseService.saveWarehouse(warehouse_DTO_to_insert);
        initial = warehouse_DTO_to_insert;
        after = response;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO
        verify(warehouseRepository, times(1)).findByAddress(any());
        verify(warehouseRepository, times(1)).save(any());

        assertEquals(
                initial.getId(),
                after.getId());
        assertEquals(
                initial.getAddress(),
                after.getAddress());
        assertEquals(
                initial.getCity(),
                after.getCity());
        assertEquals(
                initial.getSupplier().getId(),
                after.getSupplier().getId());
        assertEquals(
                initial.getSupplier().getName(),
                after.getSupplier().getName());
        assertEquals(
                initial.getSupplier().getPostalCode(),
                after.getSupplier().getPostalCode());
        assertEquals(
                initial.getSupplier().getAddress(),
                after.getSupplier().getAddress());
    }

    @Test
    @DisplayName(value = "ERROR TEST")
    public void SAVE_test_error(){
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        warehouse_DTO_to_insert = warehouseMapper.toDto(warehouse_to_insert);

        /*
         public WarehouseDTO saveWarehouse(WarehouseDTO warehouseDTO) {
         Optional<Warehouse> warehouseByAddress = warehouseRepository.findByAddress(warehouseDTO.getAddress());
         if (warehouseByAddress.isEmpty() || warehouseDTO.getId() != null) {
         Warehouse savedWarehouse = warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));
         return warehouseMapper.toDto(savedWarehouse);
         }
         throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
         }
         * */

        CustomException expectedException =  new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehouseDTO response = warehouseService.saveWarehouse(warehouse_DTO_to_insert);

        //act + assert
        CustomException exceptionResult =
                assertThrows(CustomException.class, ()->{warehouseService.saveWarehouse(warehouse_DTO_to_insert);});
        assertEquals(expectedException.getMessage(), exceptionResult.getMessage());
        verify(warehouseRepository, times(1)).findByAddress(any());
    }
    // get Warehouse

    @Test
    @DisplayName("Check if Warehouse data is received")
    void getWarehouseInfo()  {
        //set - arrange ->date care reprezinta scenariul ce se va testa
        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));
        warehouse_to_insert.setSupplier(supplier_to_insert);

        Pageable pageable = PageRequest.of(1, 1);
        WarehousePaginationResponse initial, after;

        Page<Warehouse> warehousePage = new PageImpl<Warehouse>(List.of(warehouse_to_insert), pageable, pageable.getPageSize());
        WarehousePaginationResponse warehousePaginationResponse = new WarehousePaginationResponse();
        warehousePaginationResponse.setTotalItems(warehousePage.getTotalElements());
        warehousePaginationResponse.setWarehouses(warehouseMapper.toDto(warehousePage.getContent()));

        initial = warehousePaginationResponse;

        when(warehouseRepository.findAll(pageable)).thenReturn(warehousePage);
        /*
         public WarehousePaginationResponse getWarehouses(Pageable pageable) {
             Page<Warehouse> warehousePage = warehouseRepository.findAll(pageable);

             WarehousePaginationResponse warehousePaginationResponse = new WarehousePaginationResponse();

             warehousePaginationResponse.setTotalItems(warehousePage.getTotalElements());
             warehousePaginationResponse.setWarehouses(warehouseMapper.toDto(warehousePage.getContent()));

             return warehousePaginationResponse;
         }
         * */

        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehousePaginationResponse warehouseResponse = warehouseService.getWarehouses(pageable);
        after = warehouseResponse;

        //verify / assert ->validare bazata pe ce s-a intamplat =>comparam rezultatul
        //pe obiecte de tip WarehouseDTO

        verify(warehouseRepository, times(1)).findAll(pageable);

        assertEquals(
                initial.getTotalItems(),
                after.getTotalItems());
        assertEquals(
                initial.getWarehouses().stream().count(),
                after.getWarehouses().stream().count());
    }


    // delete Warehouse

    @Test
    @DisplayName(value = "ERROR TEST")
    public void DELETE_test_error(){
        //set - arrange ->date care reprezinta scenariul ce se va testa

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();
        supplier_to_insert.setWarehouses(Set.of(warehouse_to_insert));

        warehouse_to_insert.setSupplier(supplier_to_insert);
        warehouse_DTO_to_insert = warehouseMapper.toDto(warehouse_to_insert);

        /*
         public WarehouseDTO saveWarehouse(WarehouseDTO warehouseDTO) {
         Optional<Warehouse> warehouseByAddress = warehouseRepository.findByAddress(warehouseDTO.getAddress());
         if (warehouseByAddress.isEmpty() || warehouseDTO.getId() != null) {
         Warehouse savedWarehouse = warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));
         return warehouseMapper.toDto(savedWarehouse);
         }
         throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
         }
         * */

        CustomException expectedException =  new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
        //act -> apelul metodei pe care o sa o testez => ofera un rezultat
        WarehouseDTO response = warehouseService.saveWarehouse(warehouse_DTO_to_insert);

        //act + assert
        CustomException exceptionResult =
                assertThrows(CustomException.class, ()->{warehouseService.deleteWarehouse(warehouse_DTO_to_insert.getId());});
        assertEquals(expectedException.getMessage(), exceptionResult.getMessage());
        verify(warehouseRepository, times(1)).findByAddress(any());
    }

    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}