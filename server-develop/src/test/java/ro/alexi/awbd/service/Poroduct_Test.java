package ro.alexi.awbd.service;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.SupplierDTO;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.mapper.ProductMapper;
import ro.alexi.awbd.mapper.ProductMapperImpl;
import ro.alexi.awbd.mapper.SupplierMapperImpl;
import ro.alexi.awbd.model.Product;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.ProductRepository;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class Poroduct_Test {

    @Mock
    private ProductRepository productRepo;

    @Mock
    private ProductMapper productMapper;

    @InjectMocks
    private ProductService productService;

    private Supplier supplier_to_insert;
    private Warehouse warehouse_to_insert;
    private WarehouseDTO warehouse_DTO_to_insert;
    private SupplierDTO supplierDTO_to_insert = new SupplierDTO();

    @BeforeEach
    void setUp() {
        productService = new ProductService(productRepo, productMapper);
    }

    @AfterEach
    void unset_TearDown() {
        supplier_to_insert = null;
        warehouse_to_insert = null;
        warehouse_DTO_to_insert = null;
        supplierDTO_to_insert = null;
    }


    @Test
    @DisplayName("Get Product - SUCCESS")
    void saveProduct() {
        Supplier supplier = supplier_generateTestData();

        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setProductName("banana");
        product.setPrice(10.2F);
        product.setDescription("prima banana");
        product.setImage("imgbanana");

        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductName(product.getProductName());
        productDTO.setDescription(product.getDescription());
        productDTO.setId(product.getId());
        productDTO.setImage(product.getImage());
        productDTO.setPrice(product.getPrice());


        SupplierDTO supplierDTO = new SupplierMapperImpl().toDto(supplier);
        product.setProductSupplier(supplier);
        productDTO.setProductSupplier(supplierDTO);

        /*
         public ProductDTO saveProduct(ProductDTO product) {
            Optional<Product> optionalProduct = productRepository.findFirstByProductNameAndPriceAndProductSupplierId(product.getProductName(), product.getPrice(), product.getProductSupplier().getId());

            if (optionalProduct.isEmpty() || product.getId() != null) {
                Product savedProduct = productRepository.save(productMapper.toEntity(product));

                return productMapper.toDto(savedProduct);
            } else {
                throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Produsul exista deja");
            }
        }
        * */
        when(
                productRepo.findFirstByProductNameAndPriceAndProductSupplierId(
                        productDTO.getProductName(),
                        productDTO.getPrice(),
                        productDTO.getProductSupplier().getId())
        ).thenReturn(Optional.of(product));//String productName, Float price, UUID supplierId

        when(productRepo.save(any())).thenReturn(product);
        when(productMapper.toDto(product)).thenReturn(productDTO);

        //productRepository.findFirstByProductNameAndPriceAndProductSupplierId

        ProductDTO rezultat = productService.saveProduct(productMapper.toDto(product));


        assertEquals(product.getId(),rezultat.getId());
    }



    @Test
    @DisplayName(value = "saveProduct - test_error")
    public void test_error(){
        //set
        Supplier supplier = supplier_generateTestData();

        Product product = new Product();
        product.setId(null);
        product.setProductName("banana");
        product.setPrice(10.2F);
        product.setDescription("prima banana");
        product.setImage("imgbanana");

        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductName(product.getProductName());
        productDTO.setDescription(product.getDescription());
        productDTO.setId(product.getId());
        productDTO.setImage(product.getImage());
        productDTO.setPrice(product.getPrice());


        SupplierDTO supplierDTO = new SupplierMapperImpl().toDto(supplier);
        product.setProductSupplier(supplier);
        productDTO.setProductSupplier(supplierDTO);
/*
         public ProductDTO saveProduct(ProductDTO product) {
            Optional<Product> optionalProduct = productRepository.findFirstByProductNameAndPriceAndProductSupplierId(product.getProductName(), product.getPrice(), product.getProductSupplier().getId());

            if (optionalProduct.isEmpty() || product.getId() != null) {
                Product savedProduct = productRepository.save(productMapper.toEntity(product));

                return productMapper.toDto(savedProduct);
            } else {
                throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Produsul exista deja");
            }
        }
        * */
        when(
                productRepo.findFirstByProductNameAndPriceAndProductSupplierId(
                        productDTO.getProductName(),
                        productDTO.getPrice(),
                        productDTO.getProductSupplier().getId())
        ).thenReturn(Optional.of(product));//String productName, Float price, UUID supplierId



        CustomException expectedException = new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Produsul exista deja");;

        //act + assert
        CustomException exceptionResult =
                assertThrows(CustomException.class, ()->{productService.saveProduct(new ProductMapperImpl().toDto(product));});
        assertEquals(expectedException.getMessage(), exceptionResult.getMessage());
        verify(productRepo, times(1)).findFirstByProductNameAndPriceAndProductSupplierId(productDTO.getProductName(),
                productDTO.getPrice(),
                productDTO.getProductSupplier().getId());
    }

    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}
