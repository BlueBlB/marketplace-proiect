package ro.alexi.awbd.config;

import org.hibernate.dialect.H2Dialect;
import org.springframework.context.annotation.Configuration;

import java.sql.Types;


public class H2DialectCustom extends H2Dialect {

    public  H2DialectCustom() {
        super();
        registerColumnType(Types.BINARY, "varbinary");
    }

}
