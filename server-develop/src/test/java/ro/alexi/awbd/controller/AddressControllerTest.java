package ro.alexi.awbd.controller;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import ro.alexi.awbd.AwbdApplication;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.mapper.AddressMapperImpl;
import ro.alexi.awbd.model.Address;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.*;
import ro.alexi.awbd.security.SecurityUserService;
import ro.alexi.awbd.service.AddressService;
import ro.alexi.awbd.service.WarehouseService;

import java.io.IOException;
import java.util.*;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AwbdApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@WithUserDetails(value="admin@admin.com", userDetailsServiceBeanName="securityUserService")
class AddressControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    AddressService addressService;

    @Autowired
    SecurityUserService securityUserService;

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;

   /*
   @Autowired
    SupplierRepository supplierRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    CustomerRepository customerRepository;
    */

    @BeforeEach
    void setUp() {
       /* orderRepository.deleteAll();;
        productRepository.deleteAll();
        supplierRepository.deleteAll();
        warehouseRepository.deleteAll();
        addressRepository.deleteAll();
        customerRepository.deleteAll();
        */
    }

    @Test
    void getAllAddresses() throws Exception {

        mvc.perform(get("/awbd/address/list")
                .param("pageNumber", "0")
                .param("pageSize", "10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void address_505_ID_NULL() throws Exception {

        /*
            @PostMapping("")
            public ResponseEntity<AddressDTO> createAddress(@RequestBody AddressDTO address) {
                AddressDTO savedAddress = addressService.createAddress(address);

                return new ResponseEntity<>(savedAddress, HttpStatus.ACCEPTED);
            }
        * */

        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());
        customer.setEmail("EMAIL");
        customer.setName("name");
        customer.setPassword("passwd");
        customer.setAuthority("client");
        customer.setAddresses(Set.of());
        customer.setOrders(Set.of());

        Address address = new Address();
        address.setId(UUID.randomUUID());
        address.setCity("city");
        address.setPostalCode("18454");
        address.setAddress("Adresa_Dorita");
        address.setCustomer(customer);

        AddressDTO addressDTO = new AddressMapperImpl().toDto(address);

        String jsonStr = null;
        // Creating Object of ObjectMapper define in Jackson API
        ObjectMapper Obj = new ObjectMapper();
        try {
            // Converting the Java object into a JSON string
            jsonStr = Obj.writeValueAsString(addressDTO);
            // Displaying Java object into a JSON string
            System.out.println(jsonStr);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        mvc.perform(get("/awbd/address")
                .param("address", jsonStr)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(500));
    }

    @Test
    void address_isOk() throws Exception {

        /*
            @PostMapping("")
            public ResponseEntity<AddressDTO> createAddress(@RequestBody AddressDTO address) {
                AddressDTO savedAddress = addressService.createAddress(address);

                return new ResponseEntity<>(savedAddress, HttpStatus.ACCEPTED);
            }
        * */

        Customer customer = new Customer();
        customer.setId(UUID.randomUUID());
        customer.setEmail("EMAIL");
        customer.setName("name");
        customer.setPassword("passwd");
        customer.setAuthority("client");
        customer.setAddresses(Set.of());
        customer.setOrders(Set.of());

        Address address = new Address();
        address.setId(UUID.randomUUID());
        address.setCity("city");
        address.setPostalCode("18454");
        address.setAddress("Adresa_Dorita");
        address.setCustomer(customer);

        AddressDTO addressDTO = new AddressMapperImpl().toDto(address);

        String jsonStr = null;
        // Creating Object of ObjectMapper define in Jackson API
        ObjectMapper Obj = new ObjectMapper();
        try {
            // Converting the Java object into a JSON string
            jsonStr = Obj.writeValueAsString(addressDTO);
            // Displaying Java object into a JSON string
            System.out.println(jsonStr);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        mvc.perform(post("/awbd/address")
                .param("address", jsonStr)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(500));
    }

}
