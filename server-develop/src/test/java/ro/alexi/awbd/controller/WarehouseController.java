package ro.alexi.awbd.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ro.alexi.awbd.AwbdApplication;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.mapper.WarehouseMapper;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.SupplierRepository;
import ro.alexi.awbd.repository.WarehouseRepository;
import ro.alexi.awbd.security.SecurityUserService;
import ro.alexi.awbd.service.AddressService;
import ro.alexi.awbd.service.WarehouseService;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AwbdApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@WithUserDetails(value="admin@admin.com", userDetailsServiceBeanName="securityUserService")
public class WarehouseController {
    /*
    *
    @RestController
    @RequestMapping("/awbd/warehouse")
    public class WarehouseController {

        private final WarehouseService warehouseService;

        public WarehouseController(WarehouseService warehouseService) {
            this.warehouseService = warehouseService;
        }

        @GetMapping("/list")
        public ResponseEntity<List<WarehouseDTO>> getAllWarehouses(@RequestParam int pageNumber, @RequestParam int pageSize) {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);

            WarehousePaginationResponse warehousesResponse = warehouseService.getWarehouses(pageable);

            var headers = new HttpHeaders();
            headers.add("X-Total-Count", String.valueOf(warehousesResponse.getTotalItems()));

            return new ResponseEntity<>(warehousesResponse.getWarehouses(), headers, HttpStatus.OK);

        }

        @PostMapping("")
        public ResponseEntity<WarehouseDTO> saveWarehouse(@RequestBody WarehouseDTO warehouseDTO) {
            WarehouseDTO savedWarehouse = warehouseService.saveWarehouse(warehouseDTO);

            return new ResponseEntity<>(savedWarehouse, HttpStatus.OK);
        }

        @DeleteMapping("")
        public void deleteWarehouse(@RequestParam UUID warehouseId) {
            warehouseService.deleteWarehouse(warehouseId);
        }
    }
    * */

    @Autowired
    private MockMvc mvc;

    @Autowired
    AddressService addressService;

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    SupplierRepository supplierRepository;
    @Autowired
    WarehouseMapper warehouseMapper;

    private Supplier supplier_to_insert;
    private Warehouse warehouse_to_insert;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void unset_TearDown() {
        supplierRepository.deleteAll();
        warehouseRepository.deleteAll();
    }

    @Test
    void getAll() throws Exception {

        mvc.perform(get("/awbd/warehouse/list")
                .param("pageNumber", "0")
                .param("pageSize", "10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void save_505_notLinkedBadFataProvided() throws Exception {

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();

        //not linked bad data provided

        WarehouseDTO warehouseDTO = warehouseMapper.toDto(warehouse_to_insert);

        String jsonStr = null;
        // Creating Object of ObjectMapper define in Jackson API
        ObjectMapper Obj = new ObjectMapper();
        try {
            // Converting the Java object into a JSON string
            jsonStr = Obj.writeValueAsString(warehouseDTO);
            // Displaying Java object into a JSON string
            System.out.println(jsonStr);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        mvc.perform(post("/awbd/warehouse")
                .param("warehouseDTO", jsonStr)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }


    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}
