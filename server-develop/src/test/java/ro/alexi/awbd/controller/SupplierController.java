package ro.alexi.awbd.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.jfr.Description;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ro.alexi.awbd.AwbdApplication;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.mapper.WarehouseMapper;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.SupplierRepository;
import ro.alexi.awbd.repository.WarehouseRepository;
import ro.alexi.awbd.service.AddressService;
import ro.alexi.awbd.service.WarehouseService;

import java.io.IOException;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AwbdApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.yml")
@WithUserDetails(value="admin@admin.com", userDetailsServiceBeanName="securityUserService")
public class SupplierController {
    /*
    *
     @GetMapping("/list")
    public ResponseEntity<List<SupplierDTO>> getAllWarehouses(@RequestParam int pageNumber, @RequestParam int pageSize, @RequestParam boolean completeList) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        SupplierPaginationResponse suppliersResponse = supplierService.getSuppliers(pageable, completeList);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(suppliersResponse.getTotalItems()));

        return new ResponseEntity<>(suppliersResponse.getSuppliers(), headers, HttpStatus.OK);

    }

    @PostMapping("")
    public ResponseEntity<SupplierDTO> saveWarehouse(@RequestBody SupplierDTO supplierDTO) {
        SupplierDTO savedWarehouse = supplierService.saveSupplier(supplierDTO);

        return new ResponseEntity<>(savedWarehouse, HttpStatus.OK);
    }

    @DeleteMapping("")
    public void deleteWarehouse(@RequestParam UUID supplierId) {
        supplierService.deleteSupplier(supplierId);
    }

    @GetMapping("/{parentId}/warehouses")
    public ResponseEntity<List<WarehouseDTO>> getListOfWarehousesByParent(@PathVariable("parentId") UUID parentId) {
        List<WarehouseDTO> allWarehousesBySupplierId = supplierService.findAllWarehousesBySupplierId(parentId);

        return new ResponseEntity<>(allWarehousesBySupplierId, HttpStatus.OK);
    }

    @GetMapping("/{parentId}/products")
    public ResponseEntity<List<ProductDTO>> getListOfProductsByParent(@PathVariable("parentId") UUID parentId) {
        List<ProductDTO> allProductsBySupplierId = supplierService.findAllProductsBySupplierId(parentId);

        return new ResponseEntity<>(allProductsBySupplierId, HttpStatus.OK);
    }
    * */

    @Autowired
    private MockMvc mvc;

    @Autowired
    AddressService addressService;

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    SupplierRepository supplierRepository;
    @Autowired
    WarehouseMapper warehouseMapper;

    private Supplier supplier_to_insert;
    private Warehouse warehouse_to_insert;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void unset_TearDown() {
        supplierRepository.deleteAll();
        warehouseRepository.deleteAll();
    }

    @Test
    void getAll() throws Exception {

        mvc.perform(get("/awbd/supplier/list")
                .param("pageNumber", "0")
                .param("pageSize", "10")
                .param("completeList", "true")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    void save_505_notLinked() throws Exception {

        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();

        //not linked bad data provided

        WarehouseDTO warehouseDTO = warehouseMapper.toDto(warehouse_to_insert);

        String jsonStr = null;
        // Creating Object of ObjectMapper define in Jackson API
        ObjectMapper Obj = new ObjectMapper();
        try {
            // Converting the Java object into a JSON string
            jsonStr = Obj.writeValueAsString(warehouseDTO);
            // Displaying Java object into a JSON string
            System.out.println(jsonStr);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        mvc.perform(post("/awbd/warehouse")
                .param("warehouseDTO", jsonStr)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }

    //get warehouses by supplier id
    @Test
    @Description("get getProducts by supplier id")
    void getProductsByParent() throws Exception {
        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();

        mvc.perform(get("/awbd/supplier/"+supplier_to_insert.getId()+"/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Description("get warehouses by supplier id")
    void getWarehousesByParent() throws Exception {
        supplier_to_insert = supplier_generateTestData();
        warehouse_to_insert = warehouse_generateTestData();

        mvc.perform(get("/awbd/supplier/"+supplier_to_insert.getId()+"/warehouses")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private static long randomNextGenerateDataObject = 0;
    private Supplier supplier_generateTestData() {
        randomNextGenerateDataObject++;
        Supplier supplier = new Supplier();
        supplier.setId(UUID.randomUUID());
        supplier.setName("TEST_Supplier_EMAIL"+ randomNextGenerateDataObject);
        supplier.setAddress("TEST_Supplier_NAME"+ randomNextGenerateDataObject);
        supplier.setPostalCode("TEST_Supplier_POSTAL-CODE"+ randomNextGenerateDataObject);

        return supplier;
    }

    private Warehouse warehouse_generateTestData () {
        randomNextGenerateDataObject++;
        Warehouse warehouse = new Warehouse();
        warehouse.setId(UUID.randomUUID());
        warehouse.setAddress("TEST_Warehouse_Address" + randomNextGenerateDataObject);
        warehouse.setCity("TEST_Warehouse_City" + randomNextGenerateDataObject);
        return warehouse;
    }

    private WarehouseDTO warehouseDTO_generateTestData() {
        randomNextGenerateDataObject++;
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setAddress("1_TEST_warehouseDTO_Address" + randomNextGenerateDataObject);
        warehouseDTO.setCity("1_TEST_warehouseDTO_City" + randomNextGenerateDataObject);
        return warehouseDTO;
    }
}
