package ro.alexi.awbd;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootTest
@EnableJpaRepositories({"ro.alexi.awbd"})
@EntityScan({"ro.alexi.awbd"})

public class AwbdApplicationTests {

    public static void main(String... args) {
        SpringApplication.run(AwbdApplicationTests.class, args);
    }

    @Test
    void contextLoads() {
    }

}