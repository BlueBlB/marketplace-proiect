package ro.alexi.awbd.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.model.Customer;

import java.util.UUID;

@Component
public class SecurityUtils {
    private static String jwtToken;

    public static UUID getUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();

        return customUserDetails.getId();
    }

    public static Customer getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();

        return customUserDetails.getCurrentCustomer();
    }

    public static void setJwtToken(String jwtToken) {
        SecurityUtils.jwtToken = jwtToken;
    }

    public static String getJwtToken() {
        return SecurityUtils.jwtToken;
    }
}
