package ro.alexi.awbd.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

@Service
public class SecurityUserService implements UserDetailsService {
    private final CustomerRepository customerRepository;

    public SecurityUserService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public CustomUserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Customer> byEmail = customerRepository.findByEmail(email);
        if (byEmail.isEmpty()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Utilizatorul nu a fost gasit");
        }

        Customer customer = byEmail.get();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(customer.getAuthority());
        return new CustomUserDetails(customer.getEmail(), customer.getPassword(), Collections.singleton(authority), customer.getId(), customer);
    }
}
