package ro.alexi.awbd.controller;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.service.AddressService;

import java.util.List;

@RestController
@RequestMapping("/awbd/address")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping("/list")
    public ResponseEntity<List<AddressDTO>> getAllAddresses(@RequestParam int pageNumber, @RequestParam int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        AddressPaginationResponse addresses = addressService.getAddressesByCustomer(pageable);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(addresses.getTotalItems()));

        return new ResponseEntity<>(addresses.getAddresses(), headers, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<AddressDTO> createAddress(@RequestBody AddressDTO address) {
        AddressDTO savedAddress = addressService.createAddress(address);

        return new ResponseEntity<>(savedAddress, HttpStatus.ACCEPTED);
    }
}
