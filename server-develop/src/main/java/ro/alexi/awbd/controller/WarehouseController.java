package ro.alexi.awbd.controller;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.dtos.paginationDTO.WarehousePaginationResponse;
import ro.alexi.awbd.service.WarehouseService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/awbd/warehouse")
public class WarehouseController {

    private final WarehouseService warehouseService;

    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping("/list")
    public ResponseEntity<List<WarehouseDTO>> getAllWarehouses(@RequestParam int pageNumber, @RequestParam int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        WarehousePaginationResponse warehousesResponse = warehouseService.getWarehouses(pageable);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(warehousesResponse.getTotalItems()));

        return new ResponseEntity<>(warehousesResponse.getWarehouses(), headers, HttpStatus.OK);

    }

    @PostMapping("")
    public ResponseEntity<WarehouseDTO> saveWarehouse(@RequestBody WarehouseDTO warehouseDTO) {
        WarehouseDTO savedWarehouse = warehouseService.saveWarehouse(warehouseDTO);

        return new ResponseEntity<>(savedWarehouse, HttpStatus.OK);
    }

    @DeleteMapping("")
    public void deleteWarehouse(@RequestParam UUID warehouseId) {
        warehouseService.deleteWarehouse(warehouseId);
    }
}
