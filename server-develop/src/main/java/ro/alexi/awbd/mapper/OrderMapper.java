package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.alexi.awbd.dtos.OrderDTO;
import ro.alexi.awbd.model.Order;

@Mapper(componentModel = "spring", uses = {})
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {

    @Mapping(target = "id", source = "id")
    Order toEntity(OrderDTO orderDTO);

    @Mapping(target = "id", source = "id")
    OrderDTO toDto(Order order);
}
