package ro.alexi.awbd.dtos;



import java.io.Serializable;
import java.util.List;
import java.util.UUID;



public class WarehouseDTO implements Serializable {

    private UUID id;
    private String address;
    private String city;
    private SupplierDTO supplier;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public SupplierDTO getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierDTO supplier) {
        this.supplier = supplier;
    }
}
