package ro.alexi.awbd.dtos;


import java.io.Serializable;
import java.util.List;
import java.util.UUID;


public class SupplierDTO implements Serializable {
    private UUID id;
    private String name;
    private String postalCode;
    private String address;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
