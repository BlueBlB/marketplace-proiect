package ro.alexi.awbd.dtos.enums;

public enum  AuthorityEnum {
    CUSTOMER("CUSTOMER"),
    ADMIN("ADMIN");
    private String value;

    AuthorityEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
