package ro.alexi.awbd.dtos.paginationDTO;

import ro.alexi.awbd.dtos.ProductDTO;

import java.util.List;

public class ProductPaginationResponse {

    private List<ProductDTO> products;
    private long totalItems;

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }
}
