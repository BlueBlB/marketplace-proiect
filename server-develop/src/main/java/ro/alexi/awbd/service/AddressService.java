package ro.alexi.awbd.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.mapper.AddressMapper;
import ro.alexi.awbd.model.Address;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.AddressRepository;
import ro.alexi.awbd.security.SecurityUtils;

import java.util.Optional;
import java.util.UUID;

@Service
public class AddressService {
    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressService(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    public AddressPaginationResponse getAddressesByCustomer(Pageable pageable) {
        UUID userId = SecurityUtils.getUserId();
        Page<Address> addressPage = addressRepository.findAllByCustomerId(userId, pageable);

        AddressPaginationResponse addressPaginationResponse = new AddressPaginationResponse();
        addressPaginationResponse.setAddresses(addressMapper.toDto(addressPage.getContent()));
        addressPaginationResponse.setTotalItems(addressPage.getTotalElements());
        return addressPaginationResponse;
    }

    public AddressDTO createAddress(AddressDTO address) {
        Customer currentUser = SecurityUtils.getCurrentUser();
        Optional<Address> byAddressAndCustomerId = addressRepository.findByAddressAndCustomerId(address.getAddress(), currentUser.getId());
        if (byAddressAndCustomerId.isEmpty()) {
            Address toBeSavedAddress = addressMapper.toEntity(address);
            toBeSavedAddress.setCustomer(currentUser);

            Address savedAddress = addressRepository.save(toBeSavedAddress);
            return addressMapper.toDto(savedAddress);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Adresa exista deja");
        }
    }
}
