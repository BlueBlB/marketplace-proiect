package ro.alexi.awbd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.alexi.awbd.model.Warehouse;

import java.util.Optional;
import java.util.UUID;

public interface WarehouseRepository extends JpaRepository<Warehouse, UUID> {

    Optional<Warehouse> findByAddress(String address);
}
