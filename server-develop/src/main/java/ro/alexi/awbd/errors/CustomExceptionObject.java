package ro.alexi.awbd.errors;

import org.springframework.http.HttpStatus;

public class CustomExceptionObject {

    private Integer status;
    private HttpStatus httpStatus;
    private String errorMessage;

    public CustomExceptionObject() {
    }

    public CustomExceptionObject(Integer status, String errorMessage, HttpStatus httpStatus) {
        this.status = status;
        this.errorMessage = errorMessage;
        this.httpStatus = httpStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
