import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';

@Injectable()
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {
  }


  private configSuccess: MatSnackBarConfig = {
    panelClass: ['snackbar-success'],
  };

  private configError: MatSnackBarConfig = {
    panelClass: ['snackbar-error'],
  };

  openSnackBar(message: string, action: string, isError: boolean): void {
    this.snackBar.open(message, action, {
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: isError ? this.configError.panelClass : this.configSuccess.panelClass,
      duration: 2000,
    });
  }


}
