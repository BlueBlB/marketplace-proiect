import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {SnackBarService} from '../common-services/snack-bar.service';
import {Injectable} from '@angular/core';


@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private snck: SnackBarService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Eroare client: ${error.error.message}`;
          } else {
            if (error.status === 401) {
              sessionStorage.clear();
              window.location.reload();
            }
            // server-side error
            errorMessage = `Eroare server! ${error.error.errorMessage}`;
          }
          this.snck.openSnackBar(errorMessage, 'OK', true);
          return throwError(errorMessage);
        })
      );
  }
}
