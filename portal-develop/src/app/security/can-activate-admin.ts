import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../components/login/authentication.service';
import {Authorization} from '../models/enums/authorization.enum';

@Injectable()
export class CanActivateAdmin implements CanActivate {
  constructor(private router: Router, private loginService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.loginService.isUserLoggedIn() && this.loginService.currentCustomerValue?.authority === Authorization.ADMIN) {
      return true;
    } else {
      if (this.loginService.currentCustomerValue) {
        sessionStorage.clear();
      }
      this.router.navigate(['login']);
      return false;
    }
  }
}
