import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../admin-components/products/product.service';
import {Product} from '../../../models/product.model';
import {StoreStateService} from '../../../common-services/store-state.service';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  pageSize: any = 8;
  page: any = 0;
  products: Product[];
  totalElements: any = 0;

  constructor(private productService: ProductService,
              private storeStateService: StoreStateService) {
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts(this.page, this.pageSize).subscribe(products => {
      this.totalElements = products.headers.get('X-Total-Count');
      this.products = products.body;
    });
  }

  addProduct(product: Product): void {
    this.storeStateService.addProducts(product);
  }

  changePage(pageEvent: PageEvent): void {
    this.page = pageEvent.pageIndex;
    this.getProducts();
  }
}
