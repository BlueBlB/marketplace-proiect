import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../login/authentication.service';
import {Customer} from '../../../models/customer.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AddressDialogComponent} from '../address/dialog/address-dialog/address-dialog.component';
import {AddressService} from '../address/address.service';
import {Address} from '../../../models/address.model';

@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.scss']
})
export class PersonalProfileComponent implements OnInit {

  loggedCustomer: Customer;
  customerFormGroup: FormGroup;
  displayedColumns: string[] = ['id', 'orderDetails'];

  constructor(private authService: AuthenticationService,
              private formBuilder: FormBuilder,
              private addressService: AddressService,
              private dialog: MatDialog) {
    this.authService.refreshUser();
    this.loggedCustomer = this.authService.currentCustomerValue;
  }

  ngOnInit(): void {
    this.customerFormGroup = this.formBuilder.group({
      name: [this.loggedCustomer.name ?? this.loggedCustomer.name, Validators.required],
      email: [this.loggedCustomer.email ?? this.loggedCustomer.email, Validators.required],
      phoneNumber: [this.loggedCustomer.phoneNumber ?? this.loggedCustomer.phoneNumber, Validators.required]
    });
  }

  addNewAddress(): void {
    const dialogRef = this.dialog.open(AddressDialogComponent);

    dialogRef.afterClosed().subscribe(async (res: Address) => {
      if (res) {
        await this.addressService.createAddress(res);
        await this.authService.refreshUser();
        this.loggedCustomer = this.authService.currentCustomerValue;
      }
    });
  }

  getOrdersByCustomer() {

  }
}
