import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../../login/authentication.service';
import {Address} from '../../../../../models/address.model';
import {MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-address-chooser',
  templateUrl: './address-chooser.component.html',
  styleUrls: ['./address-chooser.component.scss']
})
export class AddressChooserComponent implements OnInit {
  selectedDeliveryAddress: Address;
  addresses: Address[];
  description: string;

  constructor(public authService: AuthenticationService,
              public dialogRef: MatDialogRef<AddressChooserComponent>) {
    this.addresses = this.authService?.currentCustomerValue?.addresses;
  }

  ngOnInit(): void {
  }

  selectAndLeave(): void {
    this.dialogRef.close({deliveryAddress: this.selectedDeliveryAddress, orderDetails: this.description});
  }
}
