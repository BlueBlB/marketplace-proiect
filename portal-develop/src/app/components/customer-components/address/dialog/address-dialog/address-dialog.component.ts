import {Component, OnInit} from '@angular/core';
import {Address} from '../../../../../models/address.model';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-address-dialog',
  templateUrl: './address-dialog.component.html',
  styleUrls: ['./address-dialog.component.scss']
})
export class AddressDialogComponent implements OnInit {
  address: Address = {};

  constructor(public dialogRef: MatDialogRef<AddressDialogComponent>) {
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }
}
