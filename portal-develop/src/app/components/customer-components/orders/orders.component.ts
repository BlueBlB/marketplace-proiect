import {Component, OnInit} from '@angular/core';
import {Order} from '../../../models/order.model';
import {PageEvent} from '@angular/material/paginator';
import {OrderService} from '../cart/order.service';
import {MatDialog} from '@angular/material/dialog';
import {OrderDetailsComponent} from './dialog/order-details/order-details.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  pageSize: any = 10;
  page: any = 0;
  totalElements: any = 0;

  orders: Order[];

  constructor(private orderService: OrderService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getOrders();
  }

  viewOrder(order: Order): void {
    const dialogRef = this.dialog.open(OrderDetailsComponent, {
      data:
        {
          order,
        }
    });
  }

  changePage(pageEvent: PageEvent): void {
    this.page = pageEvent.pageIndex;

    this.getOrders();
  }

  private getOrders(): void {
    this.orderService.getOrders(this.page, this.pageSize).subscribe(value => {
      this.totalElements = value.headers.get('X-Total-Count');
      this.orders = value.body;
    });
  }
}
