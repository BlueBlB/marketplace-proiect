import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Customer} from '../../models/customer.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  readonly loginUrl = 'customer';
  private customerSubject: BehaviorSubject<Customer>;
  public currentCustomer: Observable<Customer>;

  constructor(private httpClient: HttpClient) {
    this.customerSubject = new BehaviorSubject<Customer>(JSON.parse(localStorage.getItem('currentCustomer')));
    this.currentCustomer = this.customerSubject.asObservable();
  }

  login(customer: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(this.loginUrl + '/login', customer).pipe(
      map(loggedCustomer => {
        sessionStorage.setItem('currentCustomer', JSON.stringify(loggedCustomer));
        const tokenStr = 'Bearer ' + loggedCustomer.token;
        sessionStorage.setItem('jwtToken', tokenStr);
        this.customerSubject.next(loggedCustomer);
        return loggedCustomer;
      })
    );
  }

  refreshUser(): Promise<Customer> {
    return this.httpClient.get<Customer>(this.loginUrl).pipe(
      map(loggedCustomer => {
        if (loggedCustomer) {
          sessionStorage.setItem('currentCustomer', JSON.stringify(loggedCustomer));
          this.customerSubject.next(loggedCustomer);
          return loggedCustomer;
        } else {
          sessionStorage.clear();
          this.customerSubject.next(null);
          window.location.reload();
          return;
        }

      })
    ).toPromise();
  }

  public get currentCustomerValue(): Customer {
    const customer = sessionStorage.getItem('currentCustomer');
    return customer ? JSON.parse(customer) : null;
  }

  isUserLoggedIn(): boolean {
    const currentUser = sessionStorage.getItem('currentCustomer');
    return !(currentUser === null);
  }

  logOut(): void {
    sessionStorage.clear();
    this.customerSubject.next(null);
    window.location.reload();

  }


  registerCustomer(customer: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(this.loginUrl + '/register', customer);
  }
}
