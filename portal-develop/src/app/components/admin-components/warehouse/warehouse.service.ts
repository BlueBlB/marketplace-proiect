import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Warehouse} from '../../../models/warehouse.model';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  private readonly warehouseUrl: string = 'warehouse';

  constructor(private http: HttpClient) {
  }

  getWarehouses(pageNumber, pageSize): Observable<HttpResponse<Warehouse[]>> {
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber);
    params = params.set('pageSize', pageSize);


    return this.http.get<Warehouse[]>(this.warehouseUrl + '/list', {params, observe: 'response'});
  }

  saveWarehouse(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.post<Warehouse>(this.warehouseUrl, warehouse);
  }

  deleteWarehouse(warehouse: Warehouse): Observable<Warehouse> {
    let params = new HttpParams();
    params = params.set('warehouseId', warehouse.id);

    return this.http.delete<Warehouse>(this.warehouseUrl, {params});
  }
}
