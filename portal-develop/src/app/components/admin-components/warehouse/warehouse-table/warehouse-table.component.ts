import {Component, OnInit} from '@angular/core';
import {WarehouseService} from '../warehouse.service';
import {Warehouse} from '../../../../models/warehouse.model';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {EditWarehouseDialogComponent} from '../dialogs/edit-warehouse-dialog/edit-warehouse-dialog.component';
import {SnackBarService} from '../../../../common-services/snack-bar.service';
import {DeleteSupplierComponent} from "../../supplier/dialogs/delete-supplier/delete-supplier.component";
import {DeleteWarehouseComponent} from "../dialogs/delete-warehouse/delete-warehouse.component";

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse-table.component.html',
  styleUrls: ['./warehouse-table.component.scss']
})
export class WarehouseTableComponent implements OnInit {

  warehouses: Warehouse[];
  loading = false;
  totalElements: any;
  pageSize: any = 10;
  page: any = 0;

  constructor(private warehouseService: WarehouseService,
              private dialog: MatDialog,
              private snackbarService: SnackBarService) {
  }

  ngOnInit(): void {
    this.getWarehouses();
  }

  changePage(pageEvent: PageEvent): void {
    this.page = pageEvent.pageIndex;

    this.getWarehouses();
  }

  getWarehouses(): void {
    this.loading = true;
    this.warehouseService.getWarehouses(this.page, this.pageSize).subscribe(warehouses => {
      this.totalElements = warehouses.headers.get('X-Total-Count');
      this.loading = false;
      this.warehouses = warehouses.body;
    });
  }

  editWarehouse(warehouse?: Warehouse): void {
    const toBeChange = Object.assign({}, warehouse);
    const dialogRef = this.dialog.open(EditWarehouseDialogComponent, {
      data:
        {
          warehouse: toBeChange,
          isEditing: warehouse ?? false
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.warehouseService.saveWarehouse(result).subscribe(res => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getWarehouses();
        });
      }
    });
  }

  deleteWarehouse(warehouse?: Warehouse): void {
    const warehouseToDelete = Object.assign({}, warehouse);

    const dialogRef = this.dialog.open(DeleteWarehouseComponent, {
      data: {warehouse: warehouseToDelete}
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if (res) {
        this.warehouseService.deleteWarehouse(res).subscribe(result => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getWarehouses();
        });
      }
    });
  }
}
