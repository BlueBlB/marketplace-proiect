import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WarehouseTableComponent} from './warehouse/warehouse-table/warehouse-table.component';
import {RouterModule, Routes} from '@angular/router';
import {CanActivateAdmin} from '../../security/can-activate-admin';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {EditWarehouseDialogComponent} from './warehouse/dialogs/edit-warehouse-dialog/edit-warehouse-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {DeleteWarehouseComponent} from './warehouse/dialogs/delete-warehouse/delete-warehouse.component';
import {SupplierTableComponent} from './supplier/supplier-table/supplier-table.component';
import {EditSupplierDialogComponent} from './supplier/dialogs/edit-supplier-dialog/edit-supplier-dialog.component';
import {DeleteSupplierComponent} from './supplier/dialogs/delete-supplier/delete-supplier.component';
import {MatSelectModule} from '@angular/material/select';
import {SupplierDetailListComponent} from './supplier/dialogs/supplier-detail-list/supplier-detail-list.component';
import {MatListModule} from '@angular/material/list';
import {ProductTableComponent} from './products/product-table/product-table.component';
import { EditProductComponent } from './products/dialogs/edit-product/edit-product.component';
import { DeleteProductComponent } from './products/dialogs/delete-product/delete-product.component';

const routes: Routes = [
  {
    path: 'warehouse',
    component: WarehouseTableComponent,
    canActivate: [CanActivateAdmin]
  },
  {
    path: 'supplier',
    component: SupplierTableComponent,
    canActivate: [CanActivateAdmin]
  },
  {
    path: 'products',
    component: ProductTableComponent,
    canActivate: [CanActivateAdmin]
  }
];

@NgModule({
  declarations: [WarehouseTableComponent, EditWarehouseDialogComponent, DeleteWarehouseComponent,
    SupplierTableComponent, EditSupplierDialogComponent, DeleteSupplierComponent, SupplierDetailListComponent,
    ProductTableComponent, EditProductComponent, DeleteProductComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    NgxDatatableModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    MatListModule
  ]
})

export class AdminModule {
}
