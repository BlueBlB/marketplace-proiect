import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from '../../../models/product.model';
import {Supplier} from '../../../models/supplier.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly productsUrl: string = 'products';

  constructor(protected http: HttpClient) {
  }

  getProducts(pageNumber, pageSize): Observable<HttpResponse<Product[]>> {
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber);
    params = params.set('pageSize', pageSize);


    return this.http.get<Product[]>(this.productsUrl + '/list', {params, observe: 'response'});
  }

  saveProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.productsUrl, product);
  }

  deleteProduct(product: Product): Observable<unknown> {
    let params = new HttpParams();
    params = params.set('productId', product.id);

    return this.http.delete<Supplier>(this.productsUrl, {params});
  }
}
