import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierDetailListComponent } from './supplier-detail-list.component';

describe('SupplierDetailListComponent', () => {
  let component: SupplierDetailListComponent;
  let fixture: ComponentFixture<SupplierDetailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplierDetailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
