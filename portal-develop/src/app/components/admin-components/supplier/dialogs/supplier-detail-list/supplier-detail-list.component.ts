import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Warehouse} from '../../../../../models/warehouse.model';
import {SupplierDetailsType} from '../../../../../models/enums/supplier-details-type.enum';
import {Product} from "../../../../../models/product.model";

@Component({
  selector: 'app-supplier-detail-list',
  templateUrl: './supplier-detail-list.component.html',
  styleUrls: ['./supplier-detail-list.component.scss']
})
export class SupplierDetailListComponent implements OnInit {

  warehouses: Warehouse[];
  products: Product[];
  supplierName: string;
  supplierDetailsType: SupplierDetailsType;

  SupplierDetailsType = SupplierDetailsType;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.supplierName = this.data.supplierName;
    this.supplierDetailsType = this.data.supplierType;

    if (this.supplierDetailsType === SupplierDetailsType.PRODUCT) {
      this.products = this.data.products;
    } else {
      this.warehouses = this.data.warehouses;
    }
  }

}
