import {Component, Inject, OnInit} from '@angular/core';
import {Supplier} from '../../../../../models/supplier.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-delete-warehouse',
  templateUrl: './delete-supplier.component.html',
  styleUrls: ['./delete-supplier.component.scss']
})
export class DeleteSupplierComponent implements OnInit {
  supplier: Supplier;

  constructor(
    public dialogRef: MatDialogRef<DeleteSupplierComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.supplier = this.data.supplier;
  }

}
