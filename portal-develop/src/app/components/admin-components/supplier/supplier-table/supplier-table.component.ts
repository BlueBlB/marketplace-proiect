import {Component, OnInit, ViewChild} from '@angular/core';
import {Supplier} from '../../../../models/supplier.model';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../../../common-services/snack-bar.service';
import {SupplierService} from '../supplier.service';
import {PageEvent} from '@angular/material/paginator';
import {EditSupplierDialogComponent} from '../dialogs/edit-supplier-dialog/edit-supplier-dialog.component';
import {DeleteSupplierComponent} from '../dialogs/delete-supplier/delete-supplier.component';
import {SupplierDetailListComponent} from "../dialogs/supplier-detail-list/supplier-detail-list.component";
import {SupplierDetailsType} from "../../../../models/enums/supplier-details-type.enum";

@Component({
  selector: 'app-supplier-table',
  templateUrl: './supplier-table.component.html',
  styleUrls: ['./supplier-table.component.scss']
})
export class SupplierTableComponent implements OnInit {
  @ViewChild('supplierTable') table: any;
  suppliers: Supplier[];
  loading = false;
  totalElements: any = 0;
  pageSize: any = 10;
  page: any = 0;

  constructor(private supplierService: SupplierService,
              private dialog: MatDialog,
              private snackbarService: SnackBarService) {
  }

  ngOnInit(): void {
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.loading = true;
    this.supplierService.getSuppliers(this.page, this.pageSize, false).subscribe(suppliers => {
      this.totalElements = suppliers.headers.get('X-Total-Count');
      this.loading = false;
      this.suppliers = suppliers.body;
    });
  }

  editSupplier(supplier?: Supplier): void {
    const toBeChange = Object.assign({}, supplier);
    const dialogRef = this.dialog.open(EditSupplierDialogComponent, {
      data:
        {
          supplier: toBeChange,
          isEditing: supplier ?? false
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.supplierService.saveSupplier(result).subscribe(res => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getSuppliers();
        });
      }
    });
  }

  deleteWarehouse(supplier: Supplier): void {
    const supplierToDelete = Object.assign({}, supplier);

    const dialogRef = this.dialog.open(DeleteSupplierComponent, {
      data: {supplier: supplierToDelete}
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.supplierService.deleteSupplier(res).subscribe(result => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getSuppliers();
        });
      }
    });
  }

  changePage(pageEvent: PageEvent): void {
    this.page = pageEvent.pageIndex;

    this.getSuppliers();
  }

  viewListOfWarehouses(supplier: Supplier): void {
    this.supplierService.findAllWarehousesBySupplier(supplier.id).subscribe(warehouses => {
      this.dialog.open(SupplierDetailListComponent, {
        data: {
          warehouses,
          supplierName: supplier.name,
          supplierType: SupplierDetailsType.WAREHOUSE
        }
      });
    });
  }

  viewListOfProducts(supplier: Supplier): void {
    this.supplierService.findAllProductsBySupplier(supplier.id).subscribe(products => {
      this.dialog.open(SupplierDetailListComponent, {
        data: {
          products,
          supplierName: supplier.name,
          supplierType: SupplierDetailsType.PRODUCT
        }
      });
    });
  }
}
