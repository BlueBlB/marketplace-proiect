import {Product} from './product.model';

export interface State {
  numberOfProducts?: number;
  products?: Product[];
}
