import {Supplier} from './supplier.model';

export class Warehouse {
  id?: string;
  address?: string;
  city?: string;
  supplier?: Supplier;

  constructor(init?: Partial<Warehouse>) {
    Object.assign(this, init);
  }
}
